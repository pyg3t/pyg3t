import importlib.metadata

__version__ = importlib.metadata.version("pyg3t")