PyG3T, the Python GetText Translation Toolkit, is a collection of
tools for computer assisted translation using GNU gettext.

PyG3T is released under GNU General Public License version 3 or later; see
the LICENSE and COPYING files.

Introduction
------------

PyG3T consists of the following command-line tools:

* gtcat: write catalog to standard out and change encoding
* gtcheckargs: check the translation of command line arguments heuristically
* gtcompare: print qualitative comparison of differences between two catalogs
* gtgrep: perform grep-like operations on catalogs
* gtmerge: merge translations from one catalog into another
* gtprevmsgdiff: highlight differences between old and current msgid
* gtwdiff: convert podiff to wordwise diff
* gtxml: find syntax errors in xml-formatted messages
* poabc: check for common translation errors such as missing punctuation
* podiff: generate a message-wise diff between two catalogs
* popatch: construct a catalog by patching a catalog with a podiff
* poselect: select and print messages in po-file according to criteria

Please see the --help pages for each script for further information.

The package includes the gtparse Python module which works as a
backend for the command-line tools.  This can be useful for custom
tools or scripting in general.  We do not promise that the programming
interface remains stable, but historically, it is mostly stable.
